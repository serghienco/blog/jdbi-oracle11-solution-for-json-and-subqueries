package com.serghienco.blog.oracle11workaround;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class InsertGenerator {


    public static void main(String[] args) {
        //generateAddresses("/addresses.json");
       // generateResidences();
        //generateEmployee();
    }

    private  static void  generateEmployee() {
        for(int i = 1; i <=4000; i++) {
            System.out.format("INSERT INTO TEST_USER.EMPLOYEE (ID, PERSON_ID, COMPANY_ID, LST_UPD_TS) VALUES (TEST_USER.EMPLOYEE_SEQUENCE.NEXTVAL, %d, %d, SYSDATE);\n",
                    i, i%100 + 1);
        }
    }

    private static void generateResidences() {
        //addresses 1 - 3199
        // persons 1 - 4008

        for(int i = 1; i <= 3199; i++) {
            printResidence(i, i);
        }

        for(int i = 3200, addressId = 1; i <= 3599; i++, addressId++) {
            printResidence(i, addressId);
        }

        for(int i = 3600, addressId = 1; i <= 3799; i++, addressId++) {
            printResidence(i, addressId);
        }

        for(int i = 3800, addressId = 1; i <= 3899; i++, addressId++) {
            printResidence(i, addressId);
        }

        for(int i = 3900, addressId = 1; i <= 3979; i++, addressId++) {
            printResidence(i, addressId);
        }

        for(int i = 3980, addressId = 1; i <= 4008; i++, addressId++) {
            printResidence(i, addressId);
        }

        for(int i = 1; i <= 100; i++) {
            printResidence(i*10, i*25);
        }
    }

    private static void printResidence(int persondId, int addressId) {
        System.out.format("INSERT INTO TEST_USER.RESIDENCE (ID, PERSON_ID, ADDRESS_ID, LST_UPD_TS) VALUES (TEST_USER.RESIDENCE_SEQUENCE.NEXTVAL, %d, %d, SYSDATE);\n",
                persondId, addressId);
    }

    private static void generateAddresses(String path) {

        InputStream stream = InsertGenerator.class.getResourceAsStream(path);

        try (InputStreamReader reader = new InputStreamReader(stream, Charset.forName("UTF-8"))) {

            JsonStreamParser parser = new JsonStreamParser(reader);


            while (parser.hasNext()) {

                JsonArray array = parser.next().getAsJsonArray();

                for (int i = 0; i < array.size(); i++) {

                    JsonObject object = array.get(i).getAsJsonObject();

                    if (
                            object.has("address1") &&
                                    object.has("address2") &&
                                    object.has("city") &&
                                    object.has("state") &&
                                    object.has("postalCode")) {
                        System.out.format(
                                "INSERT INTO TEST_USER.ADDRESS (ID, FIRST_LINE, SECOND_LINE, CITY, STATE, ZIPCODE, LST_UPD_TS) " +
                                        "VALUES (TEST_USER.ADDRESS_SEQUENCE.NEXTVAL, '%s', '%s', '%s', '%s', %d, SYSDATE);\n",
                                getAndEscape(object, "address1"),
                                getAndEscape(object, "address2"),
                                getAndEscape(object, "city"),
                                getAndEscape(object, "state"),
                                object.get("postalCode").getAsInt()
                        );
                    }
                }
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getAndEscape(JsonObject object, String property) {
        return object.get(property).getAsString().replaceAll("'", "''");
    }
}
